import React from "react";
import { Baju1Img } from "../../assets";
import CardItem from "../../component/Card/CardItem";
import InputDropdown from "../../component/Input/InputDropdown";

const Product = () => {
    const dataItem = [
        {
            title: "Baju Tidur",
            price: "Rp 75.0000",
            img:Baju1Img,
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
        {
            title: "Baju Tidur",
            price: "Rp 75.0000"
        },
    ]
    return (
        <div className="flex flex-col pt-10 px-32">
            <div className="w-1/5 mb-4">

            <InputDropdown placeholder="Sort By" />
            </div>
            <div className="grid grid-cols-4 gap-10">
                {dataItem?.map(prop => <CardItem {...prop} />)}
            </div>
        </div>
    )
}

export default Product