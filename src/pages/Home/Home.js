import React from "react"
import { useNavigate } from "react-router-dom"
import { Baju1Img, Baju2Img, Baju3Img, SliderPhotoImg } from "../../assets"
import Button from "../../component/Button/Button"
import CardCategory from "../../component/Card/CardCategory"
import CardItem from "../../component/Card/CardItem"
import ButtonText from "../../component/Text/ButtonText"

export default function Home() {

    const navigate = useNavigate()
    const dataCategory = [{
        title: "Casual",
        img: Baju1Img,
    }, {
        title: "Formal",
        img: Baju1Img
    },
    {
        title: "Party",
        img: Baju1Img
    },
    {
        title: "Holiday",
        img: Baju1Img
    }]

    const dataNewArrival = [
        {
            title: "Baju Anak",
            price: "100.000",
            firstImg: Baju2Img,
            secondImg: Baju3Img,
        },
        {
            title: "Baju mandi",
            price: "100.000"
        },
        {
            title: "Baju Kerja",
            price: "100.000"
        },
        {
            title: "Baju Tidur",
            price: "100.000"
        },
    ]
    return (
        <div className="2xl:px-96 lg:px-28">
            <div className="flex flex-col items-center">

                <p className="text-2xl font-bold font-allura mb-4">
                    New Arrival
                </p>
                <div className="relative">
                    <div className="w-full absolute bottom-20 flex items-center justify-center">

                        <Button classstyle="border-2 border-black " color="gray-200">
                            <ButtonText color="black" classstyle="text-lg" text="Shop Now" />
                        </Button>
                    </div>
                    <img src={SliderPhotoImg} className="mb-4" />
                </div>

                <p className="text-2xl font-bold font-allura mb-4">
                    Pick Your Own Style!
                </p>
                <div className="grid grid-cols-2 gap-3 mb-4">

                    {dataCategory?.map((prop) =>
                        <CardCategory {...prop} />
                    )}
                </div>
                <p className="text-2xl font-bold font-allura mb-4">
                    New Arrival
                </p>
                <div className="grid grid-flow-col gap-6 mb-4">
                    {dataNewArrival?.map((prop) => <CardItem onClick={() => navigate("/item-detail")} {...prop} />)}
                </div>

                <p className="text-2xl text-left self-start font-bold font-allura mb-4">
                    On Sale
                </p>
                <hr className="border-2 border-gray-400 w-full mb-4"></hr>

                <div className="grid grid-flow-col gap-6">
                    {dataNewArrival?.map((prop) => <CardItem onClick={() => navigate("/item-detail")} {...prop} />)}
                </div>
            </div>
        </div>
    )
}
