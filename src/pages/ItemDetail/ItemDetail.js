import React from "react";
import _ from "lodash"
import Card from "../../component/Card/Card";
import CardItem from "../../component/Card/CardItem";
import { useNavigate } from "react-router-dom";
import ButtonLink from "../../component/Button/ButtonLink";
import ButtonText from "../../component/Text/ButtonText";
import InputDropdown from "../../component/Input/InputDropdown";
import { Baju1Img } from "../../assets";

const ItemDetail = () => {
    const dataImg = [Baju1Img,Baju1Img,Baju1Img]
    const dataNewArrival = [
        {
            title: "Baju Anak",
            price: "100.000",
            img:Baju1Img,
        },
        {
            title: "Baju mandi",
            price: "100.000"
        },
        {
            title: "Baju Kerja",
            price: "100.000"
        },
        {
            title: "Baju Tidur",
            price: "100.000"
        },
    ]
    const navigate = useNavigate()
    return (
        <div className="container py-24">
            <div className="inline-flex mb-10">
                <div className="grid grid-flow-row pr-4 mr-4 border-r-2 border-gray-300">
                    {dataImg.map((img) =>
                        <Card className="bg-gray-300" width={162} height={230}>
                            <img src={img} />
                        </Card>
                    )}
                </div>
                <Card className="bg-gray-300 mr-36" width={541} height={769}>
                    <img src={Baju1Img} />
                </Card>
                <div className="flex flex-col">
                    <p className="text-4xl mb-10">Lorem Ipsum</p>
                    <p className="text-4xl">Rp. 100,000</p>
                    <p className="text-4xl whitespace-normal w-96">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra sit vulputate accumsan magna elementum nunc, montes, iaculis. Diam cras lobortis lorem eu. Ultrices fermentum viverra.</p>
                    <p className="text-3xl whitespace-normal w-96">Stock:100</p>
                    <InputDropdown placeholder={"Size"} />
                    <ButtonLink onClick={()=>navigate('/checkout')}>
                    <p className="text-3xl whitespace-normal">Checkout</p>
                    </ButtonLink>
                </div>
            </div>
            <p className="text-2xl text-center font-bold font-allura mb-4">
                You May Also like
            </p>
            <hr className="border-2 border-gray-400 w-full mb-4"></hr>

            <div className="grid grid-flow-col gap-6">
                {dataNewArrival?.map((prop) => <CardItem onClick={() => navigate("/item-detail")} {...prop} />)}
            </div>
        </div>
    )
}

export default ItemDetail