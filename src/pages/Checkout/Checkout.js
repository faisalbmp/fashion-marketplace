import React from "react";
import CardItemVertical from "../../component/Card/CardItemVertical";
import Input from "../../component/Input/Input";
import ButtonLink from "../../component/Button/ButtonLink";
import { useNavigate } from "react-router-dom";
import { Baju1Img } from "../../assets";

const Checkout = () => {
    const navigate = useNavigate()
    const dataItem = [
        {
            title: "baju Anak",
            price: "75.000",
            img:Baju1Img
        },
        {
            title: "baju Anak",
            price: "75.000"
        },
    ]

    return (
        <div className="px-32">
            <div className="flex flex-col items-center mb-10">
                <p className="text-3xl font-sans">Cart</p>
            </div>
            <div className="grid grid-cols-5 items-center gap-4 ">
                <p className="text-3xl text-center font-sans">Product</p>
                <p className="text-3xl text-center font-sans col-start-3">Quantity</p>
                <p className="text-3xl text-center font-sans col-start-5">Total</p>
            </div>
            <hr className="border-2 border-gray-600 mb-10 -mx-32"></hr>
            <div className="grid-flow-row grid gap-8 w-full">
                {dataItem?.map(prop =>
                    <CardItemVertical {...prop} />
                )}
            </div>
            <div className="grid grid-cols-5 items-center gap-4 mb-4">
                <p className="text-3xl text-center font-sans col-start-3">Shhipping</p>
                <p className="text-3xl text-center font-sans col-start-5">Shipping Cost</p>
            </div>
            <hr className="border-2 border-gray-600 mb-10 -mx-32"></hr>
            <div className="grid grid-cols-5 mb-10">
                <div className="grid grid-flow-col gap-4 col-span-4">
                    <Input placeholder="Province" />
                    <Input placeholder="City" />
                    <Input placeholder="Road" />
                    <Input placeholder="Zip Code" />
                </div>
                <p className="font-sans text-lg text-center col-start-5 col-end-6">Rp. 75.0000</p>
            </div>
            <p className="text-3xl text-center font-sans mb-4">Total Cost</p>
            <hr className="border-2 border-gray-600 mb-10 -mx-32"></hr>
            <p className="text-3xl text-center font-sans mb-10">Rp. 300.000 + Rp 100.000 = Rp 400.000</p>

            <div className="flex flex-row justify-center mb-10">
                <ButtonLink classstyle="border-gray-600 border-2 rounder-xl" onClick={() => navigate('/')}>
                    <p className="text-3xl whitespace-normal">Checkout</p>
                </ButtonLink>
            </div>
        </div>
    )
}

export default Checkout