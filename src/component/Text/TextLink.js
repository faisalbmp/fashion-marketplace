import React from "react";
import { useTranslation } from "react-i18next";

const TextLink = (props) => {
    const { tx, text, children, classstyle, color = "hijau_hutan" } = props
    const { t } = useTranslation();
    const translatedText = tx && t(tx)
    const content = translatedText || text || children

    return <p className={`text-${color} font-quicksand font-normal text-xs ${classstyle}`}>
        {content}
    </p>
}

export default TextLink