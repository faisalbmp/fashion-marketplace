import React from "react";

const InfoText = (props) => {
    const { label, value, className,onClick } = props
    return (
        <div className={`grid grid-rows-2 gap-0 ${className}`}>
            <p className="text-xs font-quicksand text-gray-500">{label}</p>
            <p onClick={onClick} className=" cursor-pointer text-sm font-quicksand font-bold text-hitam_2">{value}</p>
        </div>
    )
}

export default InfoText