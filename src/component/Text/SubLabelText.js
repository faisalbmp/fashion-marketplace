import React from "react";
import { useTranslation } from "react-i18next";

const SubLabelText = (props) => {
    const { tx, text, children, classstyle } = props
    const { t } = useTranslation();
    const translatedText = tx && t(tx)
    const content = translatedText || text || children

    return <p className={`text-black-300 font-normal font-quicksand text-xs ${classstyle}`}>
        {content}
    </p>
}

export default SubLabelText