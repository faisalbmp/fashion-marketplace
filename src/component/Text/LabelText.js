import classNames from "classnames";
import React from "react";
import { useTranslation } from "react-i18next";

const LabelText = (props) => {
    const { tx, text, children, classstyle } = props
    const { t } = useTranslation();
    const translatedText = tx && t(tx)
    const content = translatedText || text || children

    return <p className={`
    ${classNames({
        'font-bold font-quicksand':!classstyle?.includes('font'),
        'text-sm':!classstyle?.includes('font'),
    })}
     ${classstyle}`}>
        {content}
    </p>
}

export default LabelText