import React from "react";
import { useTranslation } from "react-i18next";

const TitleText = (props) => {
    const { tx, text, children ,classstyle} = props
    const { t } = useTranslation();
    const translatedText = tx && t(tx)
    const content = translatedText || text || children

    return <p className={`text-hitam font-quicksand font-bold text-xl ${classstyle}`}>
        {content}
    </p>
}

export default TitleText