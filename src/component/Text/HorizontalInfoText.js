import React from "react";
import ButtonLinkAddress from "../Button/ButtonLinkAddress";

const HorizontalInfoText = (props) => {
    const { label, value, valueColor = "hitam_2",onClick, labelColor = "gray-500" } = props
    return (
        <div className="grid grid-cols-12 gap-20">
            <p className={`text-sm font-quicksand col-start-1 col-end-3 text-${labelColor} justify-self-end`}>{label} <span className="ml-5">:</span></p>
            <p onClick={onClick} className={`${onClick&& 'cursor-pointer'} text-sm font-quicksand col-start-3 col-end-13 font-bold text-${valueColor} justify-self-start`}>{value}</p>
        </div>
    )
}

export default HorizontalInfoText