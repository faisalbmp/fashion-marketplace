import classNames from "classnames";
import React from "react";
import { useTranslation } from "react-i18next";

const ButtonText = (props) => {
    const { tx, text, children, classstyle, color = "white" } = props
    const { t } = useTranslation();
    const translatedText = tx && t(tx)
    const content = translatedText || text || children

    return <p className={`text-${color} ${classNames({
        'font-kameron': !classstyle?.includes('font'),
        "text-xs":!classstyle?.includes('text')
    })} ${classstyle}`}>
        {content}
    </p>
}

export default ButtonText