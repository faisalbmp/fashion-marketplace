import React from "react";
import { useTranslation } from "react-i18next";

const SectionTitleText = (props) => {
    const { tx, text, children, classstyle } = props
    const { t } = useTranslation();
    const translatedText = tx && t(tx)
    const content = translatedText || text || children

    return <p className={`font-quicksand font-semibold text-4xl text-hitam py-6 ${classstyle}`}>
        {content}
    </p>
}

export default SectionTitleText