import React from 'react'
import Button from './Button'


const ButtonLink = (props) => {
    const { children } = props
    return (
        <Button {...props} color="transparent" classstyle={`border-transparent ${props?.classstyle}`}>
            {children}
        </Button>
    )
}

export default ButtonLink