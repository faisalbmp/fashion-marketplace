import classNames from 'classnames'
import { includes } from 'lodash'
import React from 'react'

const RoundedButton = (props) => {
    const { children, className, color = "hijau_hutan" } = props
    return (
        <button {...props} className={`${
            classNames({
                "py-6":!className?.includes('py'),
                "px-3":!className?.includes('px')
            })
        }
        rounded-full bg-${color}
        ${className}`}>
            {children}
        </button>
    )
}

export default RoundedButton