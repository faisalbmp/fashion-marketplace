import classNames from 'classnames'
import React from 'react'

const Button = (props) => {
    const { children, classstyle, color = "purple-500" } = props
    return (
        <button {...props} className={`
        ${classNames({
            "px-6": !classstyle?.includes('px'),
            "py-3": !classstyle?.includes('py'),
        })}
        rounded bg-${color}
        ${classstyle}`}>
            {children}
        </button>
    )
}

export default Button