import React from "react";
import ButtonLink from "./ButtonLink";
import env from '../../config/env'
import { stubString } from "lodash";

const { USER_ADDRESS_SCANNER } = env
const ButtonLinkAddress = ({ userName, address, valueColor = "yellow-500", className }) => {
    return (
        <p className={`text-sm font-quicksand cursor-pointer font-bold text-${valueColor} ${className}`} onClick={() => window.open(`${USER_ADDRESS_SCANNER}/${address}`)}>{userName ?? address?.substring(0, 5)}...{address?.substring(address?.length - 4, address?.length)}</p>

    )
}

export default ButtonLinkAddress