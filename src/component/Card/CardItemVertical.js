import React from "react"
import { Baju1Img } from "../../assets"
import AdderInput from "../Input/AdderInput"
import Card from "./Card"

const CardItemVertical = ({ title, price,img, ...props }) => {
    return (
        <div className="grid grid-cols-5 items-center gap-4 ">
            <div className="flex flex-row items-center justify-between">
                <Card className="bg-gray-300" width={235} height={334}>
                    <img src={img} />
                </Card>
            </div>
            <div className="grid grid-flow-row gap-4">
                <p className="font-sans text-lg">{title}</p>
                <p className="font-sans text-lg"> Rp. {price}</p>
            </div>
            <div className="flex flex-col items-center">
                <AdderInput className="w-40" value={2} />
            </div>
            <p className="font-sans text-lg text-center col-start-5 col-end-6">Rp. 75.0000</p>
        </div>
    )
}

export default CardItemVertical