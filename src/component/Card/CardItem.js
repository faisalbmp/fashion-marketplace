import React, { useState } from "react"
import Card from "./Card"

const CardItem = ({ title, price, firstImg, secondImg, ...props }) => {
    const [img, setImg] = useState(firstImg)
    
    const handleMouseOver = () => {
        setImg(secondImg)
    }
    
    const handleMouseOut = () => {
        setImg(firstImg)
    }

    return (
        <div className="flex flex-col items-center cursor-pointer" onMouseOver={handleMouseOver} onMouseOut={handleMouseOut} {...props}>
            <Card className="bg-gray-300" width={235} height={334}>
                <img src={img} className="object-cover h-full w-full" />
            </Card>
            <p className="font-sans text-sm">{title}</p>
            <p className="font-sans text-lg"> Rp. {price}</p>
        </div>
    )
}

export default CardItem