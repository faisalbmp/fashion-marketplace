import classNames from "classnames";
import React from "react";


const Card = (props) => {

    const { children, width, height, className } = props
    return (
        <div style={{
            width: `${width}px`,
            height: `${height}px`
        }} className={`${classNames(
            {
                "rounded":!className?.includes('rounded'),
                'bg-gray-200': !className?.includes('bg'),
                "py-2": !className?.includes('py'),
                "px-3": !className?.includes('px'),
                "shadow-xl": !className?.includes('shadow'),
                "text-gray-800": !className?.includes('text')
            })}${className}`} {...props}>
            {children}
        </div>
    )
}

export default Card