import React from "react";
// import { Baju1Img } from "../../assets";
import Card from "./Card";

const CardCategory = ({ title, img, ...props }) => {
    return (
        <Card className="flex flex-col group items-center bg-gray-300 justify-center relative cursor-pointer" height={300} width={500} {...props}>
                <img className="object-cover h-full peer w-full" src={img} />
                <div className="w-full h-full group-hover:visible invisible bg-black absolute opacity-20" />
                <p className="font-kameron text-4xl transition ease-in-out delay-150 duration-300  group-hover:scale-150 text-stone-200 absolute top-1/2">{title}</p>
        </Card>
    )
}

export default CardCategory