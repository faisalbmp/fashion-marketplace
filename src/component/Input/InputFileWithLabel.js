import React, { useRef } from 'react'
import SpinCircleLogo from '../../assets/animation/spin_circle_logo';
import RoundedButton from '../Button/RoundedButton';
import ButtonText from '../Text/ButtonText';
import LabelText from '../Text/LabelText'
import SubLabelText from '../Text/SubLabelText';

const InputFileWithLabel = (props) => {
    const { onChange, value, loading, width,textSublabel } = props
    const inputReference = useRef();
    const fileUploadAction = () => inputReference?.current?.click();
    const changeImg = () => {
        inputReference.current.click()
    }
    return (
        <div className="flex flex-col items-center" style={{
            width: width
        }}>
            <LabelText classstyle="self-start mb-1" {...props} />
            <SubLabelText classstyle="self-start mb-4" tx={textSublabel} /> 
            <div className={`border-dashed border-gray-500 border-2 ${!inputReference?.current?.value ? "py-20" : "py-0"} w-full h-80 flex flex-col items-center ${!inputReference?.current?.value ? "justify-end" : "justify-center"}`}>
                <input
                    type="file"
                    hidden
                    ref={inputReference}
                    name="Asset"
                    className="my-4 hidden"
                    onChange={onChange}
                />
                {!inputReference?.current?.value ?
                    <>
                        <p className="font-quicksand text-xs text-hitam_2 mb-4">
                            PNG, GIF, WEBP, MP4 or MP3. Max 2MB
                        </p>
                        <RoundedButton onClick={fileUploadAction} color="transparent" className="border-hijau_hutan px-11 py-2 border-2 w-max">
                            <ButtonText color="hijau_hutan">
                                Pilih File
                            </ButtonText>
                        </RoundedButton>
                    </> : loading ?
                        <div className="flex flex-col items-center py-16">
                            <SpinCircleLogo />
                        </div> : value !== null ? <img src={value} onClick={changeImg} className='rounded cursor-pointer max-h-full max-w-full' /> 
                        : <>
                        <p className="font-quicksand text-xs text-hitam_2 mb-4">
                            PNG, GIF, WEBP, MP4 or MP3. Max 2MB
                        </p>
                        <RoundedButton onClick={fileUploadAction} color="transparent" className="border-hijau_hutan px-11 py-2 border-2 w-max">
                            <ButtonText color="hijau_hutan">
                                Pilih File
                            </ButtonText>
                        </RoundedButton>
                    </>
                        
                }
            </div>
        </div>
    )
}

export default InputFileWithLabel