import React from "react";
import { useTranslation } from "react-i18next";

const InputSwitchWithLabel = (props) => {
    const { t } = useTranslation();
    const { label, tx, value, onClick,defaultChecked,onChange } = props
    const translatedText = tx && t(tx)

    const showLabel = translatedText || label
    return (
        <label className="flex items-center cursor-pointer">
            <div className="mr-3 text-gray-700 font-medium">
                {showLabel}
            </div>
            <div className="relative">
                <input type="checkbox" {...props} defaultChecked={defaultChecked} checked={value} onChange={onChange} onClick={onClick} id="toggleB" className="sr-only" />
                <div className="block dot-bg bg-gray-200 w-14 h-8 rounded-full"></div>
                <div className="dot absolute left-1 top-1 bg-white w-6 h-6 rounded-full transition"></div>
            </div>

        </label>
    )
}

export default InputSwitchWithLabel