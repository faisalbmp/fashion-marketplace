import React, { useRef } from 'react'
import SpinCircleLogo from '../../assets/animation/spin_circle_logo';
import RoundedButton from '../Button/RoundedButton';
import ButtonText from '../Text/ButtonText';
import LabelText from '../Text/LabelText'

const InputRoundedFileWithLabel = (props) => {
    const { onChange, value, loading } = props
    const inputReference = useRef();
    const fileUploadAction = () => inputReference?.current?.click();
    const changeImg = () => {
        inputReference.current.click()
    }
    return (
        <div className="flex flex-col items-center">
            <LabelText {...props} />
            {!loading ?

                <div className={` ${!inputReference?.current?.value ? "py-4" : "py-0"} w-full flex flex-col items-center `}>
                    {!value ?
                        <div className='border-dashed border-gray-500 border-2 rounded-full h-40 w-40 mb-5'>
                            <input
                                type="file"
                                hidden
                                ref={inputReference}
                                name="Asset"
                                className="my-4 hidden"
                                onChange={onChange}
                            />

                        </div>
                        :
                        <img src={value} onClick={changeImg} className='rounded-full cursor-pointer h-24 w-24 mr-4' width='350px' />
                    }
                    <div className='flex flex-col items-center'>
                        <p className="font-quicksand text-xs font-medium text-hitam_2 mb-4">
                            PNG, GIF, WEBP, MP4 or MP3. Max 100MB
                        </p>
                        <RoundedButton onClick={fileUploadAction} color="transparent" className="border-hijau_hutan px-11 py-2 border-2 w-max">
                            <ButtonText color="hijau_hutan">
                                Pilih File
                            </ButtonText>
                        </RoundedButton>
                    </div>
                </div>
                :
                <div className="flex flex-col items-center py-16">
                    <SpinCircleLogo />
                </div>
            }
        </div>
    )
}

export default InputRoundedFileWithLabel