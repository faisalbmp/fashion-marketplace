import classNames from "classnames";
import React from "react";

const Input = (props) => {
    const { className, appendleft } = props
    return (
        <div className="inline-block relative">
            {appendleft &&
                // <div className="absolute left-3 top-3">
                <div className="absolute left-3 top-3">
                    {appendleft}
                </div>
                // </div>
            }
            <input {...props} className={`${className} w-full p-4 ${classNames({
                'shadow appearance-none border border-gray-200 rounded-md': !className?.includes('border-'),
                'pl-16': appendleft
            })}`} />
        </div>
    )
}

export default Input