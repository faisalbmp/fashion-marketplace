import classNames from 'classnames'
import React from 'react'

const InputRounded = (props) => {
    const { className, appendleft, inputstyle } = props
    return (
        <div className={`dropdown inline-block relative  ${className}`} >
            {appendleft &&
                <div className="absolute top-3 left-5" >
                    {appendleft}
                </div>
            }
            <input
                {...props}
                className={`w-full text-black border-black bg-teal-500 font-semibold 
            ${classNames({
                    "py-2": !inputstyle?.includes('py'),
                    "px-10": !inputstyle?.includes('px'),
                    "border-b-2 border-black":!inputstyle?.includes('border'),
                })}
             inline-flex items-center
             ${inputstyle}
            `}>
            </input>
        </div >
    )
}

export default InputRounded