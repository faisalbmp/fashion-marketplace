import React, { useState } from 'react'
import PropTypes from 'prop-types';
import { Slider } from 'primereact/slider';

const InputRoundedSliderDropdown = (props) => {
    const { className, onChange, value } = props
    const [show, setShow] = useState(false)
    const onChangeData = (e) => onChange(e)
    return (

        <div className={` inline-block relative ${className}`}>
            <button onClick={() => setShow(!show)} className="w-full text-black rounded-3xl border-2 border-black font-semibold py-2 px-4 inline-flex items-center  justify-between">
                <span className="mr-1">{value?.name ?? props?.placeholder}</span>
                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /> </svg>
            </button>
            <ul className={` absolute ${!show && 'hidden'}  z-30 text-gray-700 pt-1 `}>
                <div className='flex flex-col bg-white items-center py-2 px-4'>
                    <Slider value={value} onChange={(e) => onChangeData(e.value)} range className='w-72' />
                    <div className='grid grid-cols-2 gap-4 w-full mt-4'>
                        <div className='bg-gray-200 rounded-full justify-center'>
                            <p className='font-quicksand text-sm text-hitam_2 text-center py-2'> {value[0]}</p>
                        </div>
                        <div className='bg-gray-200 rounded-full justify-center'>
                            <p className='font-quicksand text-sm text-hitam_2 text-center py-2'> {value[1]}</p>
                        </div>
                    </div>
                </div>
            </ul>
        </div >
    )
}

InputRoundedSliderDropdown.propTypes = {
    searchAble: PropTypes.bool,
    selected: PropTypes.bool
}

InputRoundedSliderDropdown.defaultProps = {
    searchAble: false,
    selected: false
}
export default InputRoundedSliderDropdown