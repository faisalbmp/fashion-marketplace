import React from "react";
import { CheckboxChecklistLogoImg, CheckboxEmptyLogoImg } from "../../assets";

const ChecklistInput = ({ setValue, value = false }) => {
    return (
        <div onClick={(() => setValue(!value))}>
            {
                value ?
                    <CheckboxChecklistLogoImg />
                    :
                    <CheckboxEmptyLogoImg />
            }
        </div>
    )
}

export default ChecklistInput