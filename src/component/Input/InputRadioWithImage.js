import React from "react";
import RadioActiveIcon from "../../assets/icon/radio_active_icon";
import RadioInactiveIcon from "../../assets/icon/radio_inactive_icon";

const InputRadioWithImage = ({ logo,checked, ...props }) => {
    return (
        <div className="flex flex-row cursor-pointer items-center" onClick={async () => {
            props?.onClick(props?.value)
            
        }}>
            {checked ?
                <RadioActiveIcon className="h-6 w-6" />
                :
                <RadioInactiveIcon className="h-6 w-6" />
            }
            {logo}
        </div>
    )
}

export default InputRadioWithImage