import React from "react";
import { AuctionLogo, PriceTagLogo } from "../../assets";
import { AUCTION, FIX_PRICE } from "../../utils/constants/listedType";

const PickerListedType = ({ onChange, value, className }) => {
    const isAuction = value === AUCTION
    return (
        <div className={`grid grid-flow-col gap-4 self-start ${className}`}>
            <div onClick={() => onChange(FIX_PRICE)} className={`rounded-xl shadow-md ${onChange && "cursor-pointer"}`}>
                <div className={`${isAuction ? "inactive-picker-background" : "bg-hijau_kristal"} rounded-t-xl px-8 py-4`}>
                    <div className="w-12 h-12 circle-picker-background items-center justify-center rounded-full ">
                        <PriceTagLogo className="self-center items-center align-middle h-full w-full flex justify-center" />
                    </div>
                </div>
                <div className="pt-2 pb-4">
                    <p className="font-quicksand font-normal text-xs text-hitam text-center">
                        Harga Tetap
                    </p>
                </div>
            </div>
            <div onClick={() => onChange(AUCTION)} className={`rounded-xl shadow-md ${onChange && "cursor-pointer"}`}>
                <div className={`rounded-b-none rounded-t-xl ${isAuction ? "bg-hijau_kristal" : "inactive-picker-background"} px-8 py-4`}>
                    <div className="w-12 h-12 circle-picker-background items-center justify-center rounded-full ">
                        <AuctionLogo className="self-center items-center align-middle h-full w-full flex justify-center" />
                    </div>
                </div>
                <div className="pt-2 pb-4">
                    <p className="font-quicksand font-normal text-xs text-hitam text-center">
                        Lelang
                    </p>
                </div>
            </div>
        </div>
    )
}

export default PickerListedType