import React from "react";
import RoundedButton from "../Button/RoundedButton";
import ButtonText from "../Text/ButtonText";

const AdderInput = ({ value, onChange, handleClickSubstract, handleClickAdd, disableSubstract, disableAdder,width,className }) => {
    return (
        <div className={`flex flex-row items-center p-1 border-gray-600 border-2 ${className}`}>
            <RoundedButton
                onClick={handleClickSubstract}
                className={`py-0 px-0 h-10 w-10 border-black border-2 ${disableSubstract && 'opacity-80'} `}>
                <ButtonText color="black" tx="-" />
            </RoundedButton>
            <input
                value={value}
                onChange={onChange}
                className="shadow text-center appearance-none border w-2/5 border-gray-200 rounded-md  p-4  text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                style={{
                    width:width
                }}
            />
            <RoundedButton
                onClick={handleClickAdd}
                className={`py-0 px-0 h-10 w-10 border-black border-2 ${disableAdder && 'opacity-80'} `}>
                <ButtonText color="black" tx="+" />
            </RoundedButton>
        </div>
    )
}

export default AdderInput