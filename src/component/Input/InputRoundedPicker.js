import React from "react";
import classNames from "classnames";
import ChecklistIcon from "../../assets/icon/checklist_icon";

const InputRoundedPicker = ({ className, isActive, value, logo, text, onClick }) => {
    return (
        <div onClick={() => onClick(value)} className={`rounded-full cursor-pointer ${classNames({
            "w-full": !className?.includes("w-"),
            "py-4": !className?.includes("py-"),
            "px-8": !className?.includes("px-"),
        })} ${isActive
            ? "bg-hijau_mint border-hijau_tua border-2"
            : "bg-pink_kuning"}
             ${className}`}>
            <div className="flex flex-row justify-between items-center">
                <div className="grid grid-flow-col gap-4">
                    {logo}
                    {text}
                </div>
                {isActive && <ChecklistIcon />}
            </div>
        </div>
    )
}

export default InputRoundedPicker