import React from "react";
import NumberFormat from "react-number-format";

const InputNumber = ({ className, ...props }) => {
    return (
        <div className={className} >
            <NumberFormat
                className="shadow appearance-none border border-gray-200 rounded-md  p-4  text-gray-700 mt-2 leading-tight focus:outline-none focus:shadow-outline"
                {...props}
            />
        </div>
    )
}
export default InputNumber