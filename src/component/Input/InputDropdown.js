import React, { useEffect, useRef, useState } from 'react'
import { CheckboxChecklistLogoImg, CheckboxEmptyLogoImg, MagnifierLogoImg } from '../../assets'
import LabelText from '../Text/LabelText'
import Input from './Input'
import PropTypes from 'prop-types';
import { values } from 'lodash';

const InputDropdown = (props) => {
    const { className, searchAble, selected, onChange, value, data, initValue } = props
    const [show, setShow] = useState(false)
    const onChangeInput = (e) => onChange(e)
    const [searchInput, setSearchInput] = useState("")

    const ref = useRef();

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (!ref?.current?.contains(event.target)) {
                setShow(false);
            }
        };
        document.addEventListener("mousedown", handleClickOutside);
    }, [ref]);
    return (

        <div ref={ref} className={` inline-block relative ${className}`}>
            <button onClick={() => setShow(!show)} className={`w-full ${selected && selected.length !== 0 ? "text-white bg-black" : "text-black bg-white"}  border-2  border-black font-semibold py-2 px-4 inline-flex items-center justify-between`}>
                {
                    selected ?
                        <div className='inline-flex'>
                            <span className="mr-1">{props?.placeholder}</span>
                            {selected.length !== 0 &&
                                <span className='self-center text-xs'>({selected.length})</span>
                            }
                        </div>
                        :
                        <span className="mr-1">{value?.name ?? props?.placeholder}</span>
                }
                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /> </svg>
            </button>
            <ul className={` absolute ${!show && 'hidden'} overflow-y-auto max-h-96 z-30 text-gray-700 pt-1`}>
                {initValue &&
                    <li className="flex flex-row w-full bg-white items-center cursor-pointer py-2 px-4 " onClick={() => props?.setValue(null)}>{initValue}</li>
                }
                {data?.filter((data) => searchInput !== "" ? data.name.includes(searchInput) : true)?.map((data, key) =>
                        <li key={key} onClick={() => props?.setValue(data)} className="">
                            <p className="rounded-t bg-white hover:bg-gray-400 py-2 px-4 cursor-pointer block whitespace-no-wrap">{data.name}</p>
                        </li>
                )}
            </ul>
        </div>
    )
}

InputDropdown.propTypes = {
    searchAble: PropTypes.bool,
    selected: PropTypes.array
}

InputDropdown.defaultProps = {
    searchAble: false,
    selected: []
}
export default InputDropdown