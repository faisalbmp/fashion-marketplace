import React from 'react'
import LabelText from '../Text/LabelText'
import SubLabelText from '../Text/SubLabelText'

const TextareaWithLabel = (props) => {
    const { subLabel ,className} = props
    return (
        <div className={`flex flex-col ${className}`}>
            <LabelText {...props} />
            {subLabel &&
                <SubLabelText text={subLabel} />
            }
            <textarea {...props} className="
            shadow appearance-none border border-gray-200 rounded-md  p-4  text-gray-700 mt-2 leading-tight focus:outline-none focus:shadow-outline
            " />
        </div>
    )
}

export default TextareaWithLabel