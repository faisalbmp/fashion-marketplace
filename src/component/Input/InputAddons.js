import React from "react";

const InputAddons = ({ children, className}) => {
    return (
        <div className={`mt-auto bg-white shadow appearance-none border border-gray-200 rounded-md text-gray-700 text-center h-14 w-1/4 ml-auto ${className}`}>
            {children}
        </div>
    )
}

export default InputAddons