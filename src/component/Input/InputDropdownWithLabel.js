import React from 'react'
import { useState } from 'react'
import { CheckboxChecklistLogoImg, CheckboxEmptyLogoImg, MagnifierLogoImg } from '../../assets'
import ChevronDownIcon from '../../assets/icon/chevron_down_icon'
import LabelText from '../Text/LabelText'
import SubLabelText from '../Text/SubLabelText'
import InputRounded from './InputRounded'

const InputDropdownWithLabel = (props) => {
    // const { subLabel } = props
    const { className, searchAble, selected, onChange, value, data, subLabel, topAddon, initValue } = props

    const [show, setShow] = useState(false)
    const onChangeInput = (e) => onChange(e)
    const [searchInput, setSearchInput] = useState("")
    return (
        <div>
            <div className='mb-4'>
                <LabelText {...props} />
                {subLabel &&
                    <div className='mb-2'>
                        <SubLabelText text={subLabel} />
                    </div>
                }
            </div>
            <div className={` inline-block relative ${className}`}>
                <button onClick={() => setShow(!show)} className="w-full h-14 text-black p-4 border-2 border-gray-200 rounded-md font-semibold py-2 px-4 inline-flex items-center justify-between">
                    {
                        selected ?
                            <div className='inline-flex'>
                                <span className="mr-1">{props?.placeholder}</span>
                                {selected.length !== 0 &&
                                    <span className='self-center text-xs'>* {selected.length}</span>
                                }
                            </div>
                            :
                            <span className="mr-1">{value?.name ?? props?.placeholder}</span>
                    }
                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /> </svg>
                </button>
                <ul className={` absolute ${!show && 'hidden'} w-full border-gray-200 border-2 rounded-md z-30 text-gray-700 pt-1`}>
                    {searchAble &&
                        <li className="bg-white px-4 py-6">
                            <InputRounded
                                inputstyle="border-0 bg-gray-200"
                                onChange={(e) => setSearchInput(e.target.value)}
                                appendleft={
                                    <MagnifierLogoImg />
                                } />
                        </li>
                    }
                    {topAddon &&
                        <li className='bg-white '>
                            {topAddon}
                        </li>
                    }
                    {initValue &&
                        <li className="flex flex-row w-full bg-white items-center cursor-pointer py-2 px-4 " >{initValue}</li>
                    }
                    {data?.filter((data) => searchInput !== "" ? data.name.includes(searchInput) : true)?.map((data, key) =>
                        selected ?
                            <li key={key} className="flex flex-row w-full bg-white items-center cursor-pointer py-2 px-4 " onClick={() => onChangeInput(data)}>
                                {selected?.includes(data.value) ?
                                    <CheckboxChecklistLogoImg />
                                    :
                                    <CheckboxEmptyLogoImg />}
                                <p className="rounded-t bg-white ml-2 block whitespace-no-wrap" >{data.name}</p>
                            </li>
                            :
                            <li key={key} onClick={() => {
                                setShow(false)
                                props?.setValue(data)
                            }}>
                                <p className="rounded-t bg-white hover:bg-gray-400 py-2 px-4 cursor-pointer block whitespace-no-wrap">{data.name}</p>
                            </li>
                    )}
                </ul>
            </div >
            {/*  <select onChange={(e) => {
                props.setValue(e.target.value)
                }} className="w-full p-4 border-2 border-gray-200 rounded-md">
                <option >{props.initValue}</option>
                {props.data.map((d) =>
                    <option value={d.value}>{d.name}</option>
                )}
                <option><button></button></option>
            </select> */}
        </div>
    )
}

export default InputDropdownWithLabel