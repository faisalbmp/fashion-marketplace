import { Button } from "primereact/button";
import React, { useRef } from "react";
import styled from "styled-components";
import { contactMeImg } from "../../assets/img";
import { Toast } from 'primereact/toast';

const LayoutComponent = styled.div`
  background-color:#5EA5DD;
`

const ImageContact = styled.img`
  position:absolute;
  width:400px;
  transform: translateY(-125%);
  @media screen and (max-width: 992px) {
  width:0;
    visibility:hidden;
}
`

export const LayoutFooter = () => {
  const toastRef = useRef();

  function showInfo(value, detail) {
    toastRef.current.show({ severity: 'info', summary: value, detail: detail });
  }

  function copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
  }
  return (
    <LayoutComponent className="py-3">
      <Toast ref={toastRef} />
      <div className="d-flex flex-xl-row flex-column justify-content-xl-around justify-content-center mb-12">
        <h3 className="py-4 align-self-center">Created By Faizal Bima @ 2020</h3>
        <div className="d-flex flex-row justify-content-center  align-self-xl-end align-self-center mb-xl-0  mb-xl-1 mb-5">
          <ImageContact src={contactMeImg} />
          <Button onClick={() => window.open("https://www.instagram.com/faisalbmp/?hl=en", "_blank")} icon="fab fa-instagram" style={{ backgroundColor: "#D62976", color: "#ffffff" }} className="p-button-rounded mx-2 p-button-lg" />
          <Button onClick={() => window.open("https://id.linkedin.com/in/faizal-bima-1980ab158", "_blank")} icon="fab fa-linkedin" style={{ backgroundColor: "#0e76a8 ", color: "#ffffff" }} className="p-button-rounded mx-2 p-button-lg" />
          <Button onClick={() => {
            showInfo("Email faizal.bima.2012@gmail.com Copied", "paste to your email or save it")
            copyToClipboard("faizal.bima.2012@gmail.com")
          }}
            icon="far fa-envelope" style={{ backgroundColor: "#ffffff ", color: "#f0000f" }} className="p-button-rounded mx-2 p-button-lg" />
          <Button onClick={() => window.open("https://wa.me/6281220733270")} icon="fa fa-phone" style={{ backgroundColor: "#4BC55A ", color: "#ffffff" }} className="p-button-rounded mx-2 p-button-lg" />
        </div>
        <h3 className=" align-self-center">Powered By React <i className="fab fa-react text-primary" /> </h3>
      </div>
    </LayoutComponent>
  )
}