import React from "react";
import navSidebar from '../../router/_nav_sidebar'

const LayoutSidebar = () => {
    const createLinks = (routes) => {
        return (<ul className="list-none pl-6">
            {routes?.map((prop) => {
                if (prop?.title) {
                    return (<li className="py-4">
                        <p className="text-hitam text-base font-quicksand font-semibold">
                            {prop?.title}
                        </p>
                        {createLinks(prop?.content)}
                    </li>)
                } else {
                    return (
                        <li className="py-4">
                            <p className="text-hitam text-sm font-quicksand font-normal">
                                {prop?.name}
                            </p>
                        </li>
                    )
                }

            })}
        </ul>)
    }
    return (
        <nav className="w-72 px-6 py-12 bg-white">
            {createLinks(navSidebar)}
        </nav >

    )
}

export default LayoutSidebar