import React, { Suspense, useEffect, useState } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import routes from "../../router/routes";
import LayoutHeader from "./LayoutHeader";

function Layout(props) {
  const [routeComponent, setRouteComponent] = useState()
  const setRoute = (data) =>
    data.map((prop, key) => prop?.children ?
      <Route
        path={prop.path}
        key={key}
        element={
          <prop.component  {...props} />
        }
      >
        {setRoute(prop?.children)}
      </Route>
      : <Route
        path={prop.path}
        key={key}
        element={
          <prop.component  {...props} />
        }
      />)

  useEffect(() => {
    setRouteComponent(setRoute(routes))
  }, [])
  return (
    <>
      <LayoutHeader {...props} />
      <Suspense fallback={<div>loading...</div>}>
        <Routes >
          {routeComponent}

          <Route path="/" element={<Navigate replace to="/home" />} />
        </Routes >
      </Suspense>
      {/* <LayoutFooter {...props} /> */}
    </>
  )
}

export default Layout