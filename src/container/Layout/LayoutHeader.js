import React, { useContext, useEffect, useState, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import BagIcon from "../../assets/icon/bag_icon";
import MagnifierLogoImg from "../../assets/icon/magnifier_icon";
import Button from "../../component/Button/Button";
import ButtonLink from "../../component/Button/ButtonLink";
import InputRounded from "../../component/Input/InputRounded";
import ButtonText from "../../component/Text/ButtonText";
import navs from "../../router/_nav";

const LayoutHeader = () => {
  const navigate = useNavigate();

  const createLinks = (routes, isDropdown) => {
    return routes?.map((prop, key) => {
      if (prop.invisible) return null;
      if (prop.redirect) {
        return null;
      }
      if (isDropdown) {
        return (
          <li key={key} className="bg-white hover:bg-gray-400 py-2 px-2 block whitespace-no-wrap">

            <button
              className="
              md:mx-4 md:my-0 text-left
              "
              onClick={() => navigate(prop.url)}
            >
              <ButtonText color="black" tx={prop.name} />
            </button>
          </li>
        )
      }
      if (prop.collapse) {
        return (
          <div key={key} className="
  dropdown inline-block self-center relative
  ">

            <button
              className="
            py-3 px-2
  md:mx-4 md:my-0
"
              onClick={() => navigate(prop?.url)}
            >
              <ButtonText classstyle="font-kameron text-2xl" color="black" tx={prop.name} />
            </button>
            {<ul className="dropdown-menu absolute hidden z-30 text-gray-700 pt-1 w-full">
              {createLinks(prop.children, true)}
            </ul>}
          </div>
        );
      }
      return (
        <button
          key={key}
          className="
          py-3 px-2
  md:mx-4 md:my-0
"
          onClick={() => navigate(prop.url)}
        >
          {" "}
          <ButtonText classstyle="font-kameron text-2xl" color="black" tx={prop.name} />
        </button>
      );
    });
  };
  return (
    <div className="bg-teal-500 shadow  z-10">
      <nav className="container">
        <div
          className="
    md:flex md:justify-between md:items-center py-
  "
        >
          <div>
            <InputRounded className="col-start-7 col-end-9"
              /* onChange={(e) => setSearch(e.target.value)}
              value={search} */
              appendleft={
                <MagnifierLogoImg />
              } />
          </div>

          <ButtonLink onClick={() => navigate("/home")}>
            <p className="font-kameron text-5xl">CONTOH WEB</p>
          </ButtonLink>

          <div className="inline-flex justify-end">

            <div className="inline-flex border-r-2 border-gray-400 pr-4 mr-4">
              <ButtonLink>
                <ButtonText color="black" tx="SIGN IN" />
              </ButtonLink>

              <ButtonLink>
                <ButtonText color="black" tx="REGISTER" />
              </ButtonLink>
            </div>
            <ButtonLink>
              <BagIcon />
            </ButtonLink>
          </div>
        </div>
        <div className="inline-flex justify-between w-full ">
          {createLinks(navs)}
        </div>
      </nav>
    </div>
  );
};

export default LayoutHeader;
