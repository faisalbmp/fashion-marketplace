import React from "react";

export const HeaderText = ({ value, onClick, className }) => {
  return (
    <div onClick={onClick} className={`px-xl-5 px-3 py-5 ${className}`}>
      {value}
    </div>
  )
}