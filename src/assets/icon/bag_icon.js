import React from "react";

const BagIcon = (props) =>
    <div {...props}>
        <svg width="13" height="17" viewBox="0 0 13 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.71024 1L1 3.99763V14.4893C1 14.8869 1.12012 15.2681 1.33395 15.5492C1.54777 15.8302 1.83777 15.9882 2.14016 15.9882H10.1213C10.4237 15.9882 10.7137 15.8302 10.9275 15.5492C11.1413 15.2681 11.2615 14.8869 11.2615 14.4893V3.99763L9.55121 1H2.71024Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M1 3.99762H11.2615" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M8.41105 6.99524C8.41105 7.79026 8.1708 8.55272 7.74316 9.11489C7.31551 9.67705 6.7355 9.99287 6.13073 9.99287C5.52595 9.99287 4.94594 9.67705 4.51829 9.11489C4.09065 8.55272 3.8504 7.79026 3.8504 6.99524" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        </svg>

    </div>


export default BagIcon