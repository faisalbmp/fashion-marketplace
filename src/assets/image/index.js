import SliderPhotoImg from "./SliderPhoto.png"
import Baju1Img from "./2A8DD352-D20C-4928-A3CE-C84CE9310037.jpg"
import Baju2Img from "./64B6389C-CA16-41CD-BC8B-B7FE8D782527.jpg"
import Baju3Img from "./174809A0-93C8-4833-98BC-3860E0FA293E.jpg"

export {
    SliderPhotoImg,
    Baju1Img,
    Baju2Img,
    Baju3Img,
}