const nav = [
  {
    name: 'New',
    url: '/all-product',
    collapse: true,
  },
  {
    name: 'Clothing',
    url: '/all-product',
    collapse: true,
    children: [
      {
        name: 'Basic',
        url: '/all-product',
      },
      {
        name: 'Normal',
        url: '/all-product',
      },
      {
        name: 'Premium',
        url: '/all-product',
      },
    ]
  },
  {
    name: 'Accecories',
    url: '/all-product',
    collapse: true,
    children: [
      {
        name: 'Basic',
        url: '/all-product',
      },
      {
        name: 'Normal',
        url: '/all-product',
      },
      {
        name: 'Premium',
        url: '/all-product',
      },
    ]
  },
  {
    name: 'Sale',
    url: '/all-product',
    collapse: true,
  },
  {
    name: 'Contact Us',
    url: '/all-product',
    collapse: true,
  },
]
export default nav