import Home from '../pages/Home/Home';
import ItemDetail from '../pages/ItemDetail/ItemDetail';
import Checkout from '../pages/Checkout/Checkout';
import Product from '../pages/Product/Product';


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/home', name: 'Home', component: Home/*  , layout: "" */ },
  { path: '/item-detail', name: 'Item Detail', component: ItemDetail/*  , layout: "" */ },
  { path: '/checkout', name: 'Chekcout', component: Checkout/*  , layout: "" */ },
  { path: '/all-product', name: 'Product', component: Product/*  , layout: "" */ },
];

export default routes;
