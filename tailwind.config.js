module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true
    },
    extend: {
      colors: {
        hitam: '#222222',
        hitam_2: '#3C3C3B',
        gradasi_hitam: 'linear-gradient(180deg, rgba(0, 0, 0, 0.56) 0%, rgba(0, 0, 0, 0.91) 37.41%, #000000 100%)',
        hijau_hutan: '#4AA82D',
        hijau_tua: '#33711F',
        hijau_kristal: '#AADC9B',
        hijau_misty: '#108775',
        pink_kuning: '#FAFAFF',
        hijau_mint: 'rgba(180, 255, 157, 0.1);'
      },
      fontFamily: {
        kameron: ["Kameron"], // ! find out why this fontFamily still not register to the styles.css inside the public -o output folder.
        allura: ["Allura"], // ! find out why this fontFamily still not register to the styles.css inside the public -o output folder.
      },
      height: {
        sm: '8px',
        md: '16px',
        lg: '24px',
        xl: '48px',
        fileHeight: '331px'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
